# BitBucket for Ferdi

An unofficial Ferdi recipe for Atlassian's BitBucket 

## Known Issues

* **After logging in, Ferdi throws an error about excessive logins and doesn't proceed further**

- Just right click on the BitBucket logo and select `Reload`, this is only a problem during the login process
